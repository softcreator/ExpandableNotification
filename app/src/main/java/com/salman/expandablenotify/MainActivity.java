package com.salman.expandablenotify;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class MainActivity extends Activity {
    private final static String sample_url = "http://i.imgur.com/QGyONgY.png";

    private final static int NORMAL = 0x00;
    private final static int BIG_TEXT_STYLE = 0x01;
    private final static int BIG_PICTURE_STYLE = 0x02;
    private final static int INBOX_STYLE = 0x03;
    private final static int CUSTOM_VIEW = 0x04;



    private static NotificationManager mNotificationManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


       /* FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
    }

    public void setNormalStyle(View view) {
        new CreateNotification(NORMAL).execute();
    }

    /*public void setBigTextStyle(View view) {
        new CreateNotification(BIG_TEXT_STYLE).execute();
    }*/

    public void setBigPictureStyle(View view) {
        new CreateNotification(BIG_PICTURE_STYLE).execute();
    }

    /*public void setInboxStyle(View view) {
        new CreateNotification(INBOX_STYLE).execute();
    }
*/
    /*public void setCustomView(View view) {
        new CreateNotification(CUSTOM_VIEW).execute();
    }*/

    public class CreateNotification extends AsyncTask<Void, Void, Void> {

        int style = NORMAL;

        /**
         * Main constructor for AsyncTask that accepts the parameters below.
         *
         * @param style {@link #NORMAL}, {@link #BIG_TEXT_STYLE}, {@link #BIG_PICTURE_STYLE}, {@link #INBOX_STYLE}
         * @see #doInBackground
         */
        public CreateNotification(int style) {
            this.style = style;
        }


        @Override
        protected Void doInBackground(Void... params) {
            Notification noti = new Notification();


            switch (style) {
                case NORMAL:
                    noti = setNormalNotification();
                    break;

               /* case BIG_TEXT_STYLE:
                    noti = setBigTextStyleNotification();
                    break;*/

                case BIG_PICTURE_STYLE:
                    noti = setBigPictureStyleNotification();
                    break;

                /*case INBOX_STYLE:
                    noti = setInboxStyleNotification();
                    break;*/

               /* case CUSTOM_VIEW:
                    noti = setCustomViewNotification();
                    break;
*/
            }
            noti.defaults |= Notification.DEFAULT_LIGHTS;
            noti.defaults |= Notification.DEFAULT_VIBRATE;
            noti.defaults |= Notification.DEFAULT_SOUND;

            noti.flags |= Notification.FLAG_ONLY_ALERT_ONCE;

            mNotificationManager.notify(0, noti);

            return null;
        }
    }


    private Notification setNormalNotification() {

                Bitmap remote_picture = null;

                try {
                    remote_picture = BitmapFactory.decodeStream((InputStream) new URL(sample_url).getContent());
                } catch (IOException e) {
                    e.printStackTrace();
                }

                // Setup an explicit intent for an ResultActivity to receive.
                Intent resultIntent = new Intent(this, ResultActivity.class);

                // TaskStackBuilder ensures that the back button follows the recommended convention for the back key.
                TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);

                // Adds the back stack for the Intent (but not the Intent itself).

                stackBuilder.addParentStack(ResultActivity.class);
//            Adds the Intent that starts the Activity to the top of the stack.

                    stackBuilder.addNextIntent(resultIntent);

            PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);



                  return new NotificationCompat.Builder(this)
                          .setSmallIcon(R.drawable.notification_icon)
                          .setAutoCancel(true)
                          .setLargeIcon(remote_picture)
                          .setContentIntent(resultPendingIntent)
                          .addAction(R.drawable.ic_stat_dismiss, "One", resultPendingIntent)
                          .addAction(R.drawable.ic_stat_snooze, "Two", resultPendingIntent)
                          .addAction(R.drawable.ic_stat_snooze, "Three", resultPendingIntent)
                          .setContentTitle("Normal Notification")
                          .setContentText("This is an example of a Normal Style.").build();

        }

        private Notification setBigPictureStyleNotification() {
        Bitmap remote_picture = null;

        // Create the style object with BigPictureStyle subclass.
        NotificationCompat.BigPictureStyle notiStyle = new NotificationCompat.BigPictureStyle();
        notiStyle.setBigContentTitle("Big Picture Expanded");
        notiStyle.setSummaryText("Nice big picture.");

        try {
            remote_picture = BitmapFactory.decodeStream((InputStream) new URL(sample_url).getContent());
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Add the big picture to the style.
        notiStyle.bigPicture(remote_picture);

        // Creates an explicit intent for an ResultActivity to receive.
        Intent resultIntent = new Intent(this, ResultActivity.class);

        // This ensures that the back button follows the recommended convention for the back key.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);

        // Adds the back stack for the Intent (but not the Intent itself).
        stackBuilder.addParentStack(ResultActivity.class);

        // Adds the Intent that starts the Activity to the top of the stack.
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        return new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.notification_icon)
                .setAutoCancel(true)
                .setLargeIcon(remote_picture)
                .setContentIntent(resultPendingIntent)
                .addAction(R.drawable.ic_stat_dismiss, "One", resultPendingIntent)
                .addAction(R.drawable.ic_stat_snooze, "Two", resultPendingIntent)
                .addAction(R.drawable.ic_stat_snooze, "Three", resultPendingIntent)
                .setContentTitle("Big Picture Normal")
                .setContentText("This is an example of a Big Picture Style.")

                        .setStyle(notiStyle).build();


        }


}



